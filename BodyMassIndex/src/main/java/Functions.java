import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.lang3.math.NumberUtils;

public class Functions {
	/**
	 * get parameters from console
	 * 
	 * @throws IOException
	 */
	public static void getParameters() throws IOException {
		String height = "";
		String heightUnit = "";
		String weight = "";
		String weightUnit = "";
		String sex = "";
		
		double weightNumber;
		double heighttNumber;
		while (!checkParameter(1, sex, "")) {
			try {
				InputStreamReader ir = new InputStreamReader(System.in);
				BufferedReader br = new BufferedReader(ir);
				System.out.println("Please enter your sex (male or female): ");
				sex = br.readLine();

				if (checkParameter(1, sex, "")) {
					while (!checkParameter(2, height, heightUnit)) {
						try {
							System.out.println(
									"Please enter your height and unit separated by Enter (e.g. 165 (enter) cm): ");
							height = br.readLine();
							height = height.replace(',', '.');
							heightUnit = br.readLine();

							if (checkParameter(2, height, heightUnit)) {
								while (!checkParameter(3, weight, weightUnit)) {
									try {
										System.out.println(
												"Please enter your weight and unit separated by Enter (e.g. 55 (enter) kg): ");
										weight = br.readLine();
										weightUnit = br.readLine();
										if(checkParameter(3, weight, weightUnit)) {
											weightNumber = changeWeightrUnit(weight, weightUnit);
											heighttNumber= changeHeightrUnit(height, heightUnit);
											BMI index = new BMI(heighttNumber, weightNumber, sex);
											System.out.println(index);
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							} else {
								height = "";
								heightUnit = "";
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	
	/**
	 * check argument parameter
	 * 
	 * @param paramtype checking type
	 * @param param     sex, weight, height
	 * @param unit      weight and height unit
	 * @return parameter is valid
	 */
	@SuppressWarnings("deprecation")
	private static boolean checkParameter(int paramtype, String param, String unit) {
		boolean parameterIsOk = false;
		if (paramtype > 0 && param != "") {
			ArrayList<String> weightUnit = new ArrayList<String>();
			ArrayList<String> heightUnit = new ArrayList<String>();

			weightUnit.add(0, "g");
			weightUnit.add(1, "dkg");
			weightUnit.add(2, "kg");

			heightUnit.add(0, "mm");
			heightUnit.add(1, "cm");
			heightUnit.add(2, "dm");
			heightUnit.add(3, "m");

			switch (paramtype) {
			case 1:
				if (param.equals("male") || param.equals("female")) {
					parameterIsOk = true;
				} else {
					parameterIsOk = false;
				}
				break;
			case 2:
				if (NumberUtils.isNumber(param)) {
					if (heightUnit.contains(unit)) {
						parameterIsOk = true;
					} else {
						System.out.println("Inadequate unit. Try again!");
						parameterIsOk = false;
					}
				} else {
					System.out.println("Inadequate number format. Try again!");
					parameterIsOk = false;
				}

				break;
			case 3:
				if (NumberUtils.isNumber(param)) {
					if (weightUnit.contains(unit)) {
						parameterIsOk = true;
					} else {
						System.out.println("Inadequate unit1. Try again!");
						parameterIsOk = false;
					}
				} else {
					System.out.println("Inadequate number format1. Try again!");
					parameterIsOk = false;
				}
				break;
			default:
				parameterIsOk = false;
				break;
			}
		}
		return parameterIsOk;
	}
	
	/**
	 * Changing weight to kilogram
	 * 
	 * @param w     weight
	 * @param wUnit weight unit
	 * @return changed weight
	 */
	private static double changeWeightrUnit(String w, String wUnit) {
		double retWeight = 0;
		double weight = Double.parseDouble(w);
		
		ArrayList<String> weightUnit = new ArrayList<String>();
		weightUnit.add(0, "g");
		weightUnit.add(1, "dkg");
		weightUnit.add(2, "kg");


		int weightIndex = weightUnit.indexOf(wUnit);
		
		switch (weightIndex) {
		case 0:
			retWeight = (weight/1000);
			break;
		case 1:
			retWeight = (weight/100);
			break;
		case 2:
			retWeight = weight;
			break;
		}
		return retWeight;
	}
	
	/**
	 * Changing height to meter
	 * 
	 * @param h      height
	 * @param hUnit  height unit
	 * @return changed height
	 */
	private static double changeHeightrUnit(String h, String hUnit) {
		double retHeight = 0;
		double height = Double.parseDouble(h);
		
		ArrayList<String> heightUnit = new ArrayList<String>();
		heightUnit.add(0, "mm");
		heightUnit.add(1, "cm");
		heightUnit.add(2, "dm");
		heightUnit.add(3, "m");
		
		int heightIndex = heightUnit.indexOf(hUnit);
		
		switch (heightIndex) {
		case 0:
			retHeight = (height/1000);
			break;
		case 1:
			retHeight = (height/100);
			break;
		case 2:
			retHeight = (height/10);
			break;
		case 3:
			retHeight = height;
			break;
		}
		return retHeight;
		
	}
}
