import org.apache.commons.math3.util.Precision;

public class BMI {
	double height;
	double weight;
	String sex;
	
	public BMI(double height, double weight, String sex) {
		super();
		this.height = height;
		this.weight = weight;
		this.sex = sex;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	

	public double BMIindex() {
		double index = 0;
		
		index = Precision.round(this.weight/(Math.pow(this.height, 2)), 2);
		return index;
	}
	
	public String IsOkBMI(double index) {
		String text ="";
		if(index<16) {
			text ="Underweight";
		} else if (index>=16 && index<16.99) {
			text ="Underweight";
		} else if (index>=17 && index<18.49) {
			text ="Underweight";
		} else if (index>=18.5 && index<24.99) {
			text ="Normal";
		} else if (index>=25 && index<29.99) {
			text ="Overweight";
		} else if (index>=30 && index<34.99) {
			text ="Obese";
		} else if (index>=35 && index<39.99) {
			text ="Obese";
		} else if (index>=40) {
			text ="Extremely obese";
		}
		return text;
	}
	@Override
	public String toString() {
		return "Your BMI index is: "+BMIindex()+"\n"+IsOkBMI(BMIindex());
	}
	

}
